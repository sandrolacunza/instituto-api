<?php

use Illuminate\Http\Request; 
use Illuminate\Support\Facades\Route;
use App\Models\Article;
use App\Http\Controllers\ArticleController;
use App\Http\Controllers\ApiController;
use App\Http\Controllers\CommentController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\TeacherController;
use App\Http\Controllers\ContactController;
use App\Http\Controllers\CourseController;
use App\Http\Controllers\LectivoController;
use App\Http\Controllers\CursadaController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\RolesPermissionController;
use App\Http\Controllers\InscriptionController;
use App\Http\Controllers\PagoController;
use App\Http\Controllers\NoteController;
use App\Http\Controllers\VerificationController;
use App\Http\Controllers\ForgotPasswordController;
use App\Http\Controllers\ResetPasswordController;
/*

|--------------------------------------------------------------------------

| API Routes

|--------------------------------------------------------------------------

|

| Here is where you can register API routes for your application. These

| routes are loaded by the RouteServiceProvider within a group which

| is assigned the "api" middleware group. Enjoy building your API!

|

*/



// Route::get('install', [ApiController::class, 'getMigrar']);

//Verify Email

// Email Verification Routes...
Route::get('email/verify', [VerificationController::class, 'show'])->name('verification.notice');
Route::get('email/verify/{id}', [VerificationController::class, 'verify'])->name('verification.verify');
Route::get('email/resend/{id}', [VerificationController::class, 'resend'])->name('verification.resend');



Route::post('login', [UserController::class, 'authenticate']);


Route::get('articles', [ArticleController::class, 'index']);
Route::get('articles/{article}', [ArticleController::class, 'show']);
Route::get('articles/{article}/image', [ArticleController::class, 'image']);
Route::get('articles/{article}/comments', [CommentController::class, 'index']);
//ruta para recibir contactos generales de los clientes
Route::post('contact', [ContactController::class, 'store']);
Route::post('contact/job', [ContactController::class, 'messagejobs']);
//categories
Route::get('category', [CategoryController::class, 'index']);
Route::get('category/{category}', [CategoryController::class, 'show']);
Route::get('category/{category}/image', [CategoryController::class, 'image']);
Route::post('user/refresh', [UserController::class, 'refresh']);

Route::post('/reset-password-request', [ForgotPasswordController::class, 'sendPasswordResetEmail']);
Route::post('/change-password', [ResetPasswordController::class, 'passwordResetProcess']);

Route::group(['middleware' => ['jwt.verify']], function () {

    Route::post('divisions', [CursadaController::class, 'division']);
    Route::get('teachers', [TeacherController::class, 'index']);
    Route::get('teachers/courses', [TeacherController::class, 'myCourses']);
    Route::get('teachers/{cursada}/inscriptions', [TeacherController::class, 'inscriptions']);
    Route::get('cursada/{inscription}/notes', [NoteController::class, 'index']);
    Route::put('note/{note}', [NoteController::class, 'update']);
    //permissions and roles
    Route::get('permisos/{role}', [RolesPermissionController::class, 'show'])->name('roles.show')->middleware('permission:roles.show');
    Route::get('roles', [RolesPermissionController::class, 'index'])->name('roles.index')->middleware('permission:roles.index');
    //users
    Route::post('register', [UserController::class, 'register'])->name('users.register')->middleware('permission:users.register');
    Route::get('users', [UserController::class, 'index'])->name('users.index')->middleware('permission:users.index');
    Route::put('user/{user}', [UserController::class, 'update'])->name('users.update')->middleware('permission:users.update');
    Route::delete('user/{user}', [UserController::class, 'delete'])->name('users.delete')->middleware('permission:users.delete');
    Route::post('entorno', [UserController::class, 'setenv'])->name('users.entorno')->middleware('permission:users.entorno'); //Modifify enviroment
    Route::get('user/{user}/avatar', [UserController::class, 'avatar'])->name('users.avatar')->middleware('permission:users.avatar'); //(ruta en común)
    Route::post('user/perfil', [UserController::class, 'perfil'])->name('users.perfil')->middleware('permission:users.perfil'); //(ruta en común) the user can update your own perfil
    Route::post('user/perfil/avatar', [UserController::class, 'updateImage'])->name('users.updateImage')->middleware('permission:users.updateImage'); //(ruta en común)
    Route::get('user/cursadas', [UserController::class, 'mycursadas'])->name('users.mycursadas')->middleware('permission:users.mycursadas'); //(ruta en común) get student's courses
    Route::get('user', [UserController::class, 'getAuthenticatedUser']);
    Route::post('logout', [UserController::class, 'logout']);
    //Articles
    Route::post('articles', [ArticleController::class, 'store'])->name('articles.store')->middleware('permission:articles.store');
    Route::put('articles/{article}', [ArticleController::class, 'update'])->name('articles.update')->middleware('permission:articles.update');
    Route::delete('articles/{article}', [ArticleController::class, 'delete'])->name('articles.delete')->middleware('permission:articles.delete');
    // Comments
    Route::get('articles/{article}/comments/{comment}', [CommentController::class, 'show'])->name('comments.show')->middleware('permission:comments.show');
    Route::put('comments/{comment}', [CommentController::class, 'update'])->name('comments.update')->middleware('permission:comments.update');
    Route::post('articles/{article}/comments', [CommentController::class, 'store'])->name('comments.store')->middleware('permission:comments.store'); //(ruta en común)
    Route::delete('/comments/{comment}', [CommentController::class, 'delete'])->name('comments.delete')->middleware('permission:comments.delete'); //(ruta en común)
    //course 
    Route::post('course', [CourseController::class, 'store'])->name('course.store')->middleware('permission:course.store');
    Route::get('course', [CourseController::class, 'index'])->name('course.index')->middleware('permission:course.index');
    Route::put('course/{course}', [CourseController::class, 'update'])->name('course.update')->middleware('permission:course.update');
    Route::delete('course/{course}', [CourseController::class, 'delete'])->name('course.delete')->middleware('permission:course.delete');
    //lectivo
    Route::post('lectivo', [LectivoController::class, 'store'])->name('lectivo.store')->middleware('permission:lectivo.store');
    Route::get('lectivo', [LectivoController::class, 'index'])->name('lectivo.index')->middleware('permission:lectivo.index');
    Route::delete('lectivo/{lectivo}', [LectivoController::class, 'delete'])->name('lectivo.delete')->middleware('permission:lectivo.delete');
    //cursada
    Route::post('cursada', [CursadaController::class, 'store'])->name('cursada.store')->middleware('permission:cursada.store');
    Route::get('cursada', [CursadaController::class, 'index'])->name('cursada.index')->middleware('permission:cursada.index');
    Route::get('cursada/{cursada}', [CursadaController::class, 'show'])->name('cursada.show')->middleware('permission:cursada.show');
    Route::put('cursada/{cursada}', [CursadaController::class, 'update'])->name('cursada.update')->middleware('permission:cursada.update');
    Route::delete('cursada/{cursada}', [CursadaController::class, 'delete'])->name('cursada.delete')->middleware('permission:cursada.delete');
     //categories
    Route::post('category', [CategoryController::class, 'store'])->name('category.store')->middleware('permission:category.store');
    Route::put('category/{category}', [CategoryController::class, 'update'])->name('category.update')->middleware('permission:category.update');
    Route::delete('category/{category}', [CategoryController::class, 'delete'])->name('category.delete')->middleware('permission:category.delete');

    //inscriptions
    Route::get('inscription', [InscriptionController::class, 'index'])->name('inscription.index')->middleware('permission:inscription.index');
    Route::post('inscription', [InscriptionController::class, 'store'])->name('inscription.store')->middleware('permission:inscription.store');
    Route::put('inscription/{inscription}', [InscriptionController::class, 'update'])->name('inscription.update')->middleware('permission:inscription.update');
    Route::delete('inscription/{inscription}', [InscriptionController::class, 'delete'])->name('inscription.delete')->middleware('permission:inscription.delete');
    //pagos
    Route::get('pago', [PagoController::class, 'index'])->name('pagos.index')->middleware('permission:pagos.index');
    Route::post('pago', [PagoController::class, 'store'])->name('pagos.store')->middleware('permission:pagos.store');
    Route::put('pago/{pago}', [PagoController::class, 'update'])->name('pagos.update')->middleware('permission:pagos.update');
    Route::delete('pago/{pago}', [PagoController::class, 'delete'])->name('pagos.delete')->middleware('permission:pagos.delete');
    Route::post('cuotas', [PagoController::class, 'create'])->name('pagos.create')->middleware('permission:pagos.create'); //generar cuotas del mes
});

