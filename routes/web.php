<?php



use Illuminate\Support\Facades\Route;
use App\Mail\SendMail;
use App\Models\User;

/*

|--------------------------------------------------------------------------

| Web Routes

|--------------------------------------------------------------------------

|

| Here is where you can register web routes for your application. These

| routes are loaded by the RouteServiceProvider within a group which

| contains the "web" middleware group. Now create something great!

|

*/

Route::middleware(['auth'])->group(function () {

    Route::get('/prueba', function () {
        return view('welcome')->middleware('permission:articles.destroy');;
    
    });
});


Route::get('/', function () {

    return view('welcome');

});


Route::get('probando', function() {
    $token = 'liuhgliuhliuhiuh';
    $user = User::whereEmail('pepitolacascabel@gmail.com')->first();
    return new SendMail($token, $user);
    });
    
Route::get('email', function () {

    return view('emails/message-userregister');

});