<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notes', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('inscription_id')->nullable();
            $table->foreign('inscription_id')->references('id')->on('cursada_user')->onDelete('set null');
            $table->unsignedBigInteger('lectora')->nullable();
            $table->unsignedBigInteger('auditiva')->nullable();
            $table->unsignedBigInteger('escrita')->nullable();
            $table->unsignedBigInteger('oral')->nullable();
            $table->unsignedBigInteger('usenglish')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notes');
    }
}
