<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePagosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pagos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('cursada_id')->nullable();
            $table->foreign('cursada_id')->references('id')->on('cursada')->onDelete('cascade');
            $table->unsignedBigInteger('user_id')->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->enum('concepto', array('0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12'));
            $table->decimal('subtotal', 7, 2);
            $table->decimal('descuento', 3, 2)->default(1,00);
            $table->decimal('total', 7, 2);
            $table->enum('estado', array('0', '1', '2'))->default('0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pagos');
    }
}
