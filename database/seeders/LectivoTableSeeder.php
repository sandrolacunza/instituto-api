<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Lectivo;

class LectivoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Lectivo::truncate();
        $faker = \Faker\Factory::create();
        // Crear la misma clave para todos los usuarios
        // conviene hacerlo antes del for para que el seeder
        // no se vuelva lento.
        
        // Generar algunos usuarios para nuestra aplicacion
        for ($i = 2010; $i < 2019; $i++) {
            $inicio = date('Y/m/d', strtotime($i.'/01/26'));
            $fin = date('Y/m/d', strtotime($i.'/11/26'));
            $lectivo = Lectivo::create([
                'ano' => $i,
                'inicio' => $inicio,
                'fin' => $fin,
            ]);
        }
    }
}
