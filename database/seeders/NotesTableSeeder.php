<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Note;
use App\Models\Inscription;
class NotesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Note::truncate();
        $faker = \Faker\Factory::create();
        $inscriptions = Inscription::all();
        foreach ($inscriptions as $inscription) {
            for ($i=0; $i < 5; $i++) { 
                Note::create([
                    'inscription_id' => $inscription->id
                ]);
            };
           
        };
    }
}
