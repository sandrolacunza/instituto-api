<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Schema;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        Schema::disableForeignKeyConstraints();
        $this->call(CategoriesTableSeeder::class);
        $this->call(PermissionsTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(ArticleTableSeeder::class);
        $this->call(CommentsTableSeeder::class);
        $this->call(CourseTableSeeder::class);
        $this->call(LectivoTableSeeder::class);
        $this->call(CursadaTableSeeder::class);
        $this->call(InscriptionTableSeeder::class);
        $this->call(NotesTableSeeder::class);
        Schema::enableForeignKeyConstraints();
    }
}

