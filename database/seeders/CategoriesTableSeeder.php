<?php



namespace Database\Seeders;



use Illuminate\Database\Seeder;

use App\Models\Category;

class CategoriesTableSeeder extends Seeder

{

    /**

     * Run the database seeds.

     *

     * @return void

     */

    public function run()

    {

        //

        Category::truncate();

        $faker = \Faker\Factory::create();

        for ($i = 0; $i < 3; $i++) {

            Category::create([

            'name' => $faker->word,
            'description' => $faker->paragraph,
            'image' => 'cc3cecb61224623f3a2e11f97fffeb95.png'
            ]);

        }

    }

}

