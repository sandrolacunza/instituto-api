<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Cursada;
use App\Models\Course;
use App\Models\Lectivo;
class CursadaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Vaciar la tabla
        Cursada::truncate();
        $faker = \Faker\Factory::create();
        // Crear la misma clave para todos los usuarios
        // conviene hacerlo antes del for para que el seeder
        // no se vuelva lento.
        $lectivo = Lectivo::all();
        $course = Course::all();
        foreach ($course as $c) {
            foreach ($lectivo as $l) {
                Cursada::create([
                   'courses_id' => $c->id,
                   'lectivo_id' => $l->id,
                   'estado' => '1',
                   'division' => 'A',
                   'teacher_id' => 3,
                   'price' => $faker->numberBetween(1000, 5000),
                   'inicio' => '2021/02/01',
                   'fin' => '2021/11/30'
                    ]);
            }
        }
        
    }
}

