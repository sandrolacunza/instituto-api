<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\Models\User;
use Spatie\Permission\Traits\HasRoles;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    use HasRoles;
    
    public function run()
    {
         //Permission list
         Permission::create(['name' => 'roles.show']);
         Permission::create(['name' => 'roles.index']);

         Permission::create(['name' => 'users.register']);
         Permission::create(['name' => 'users.index']);
         Permission::create(['name' => 'users.update']);
         Permission::create(['name' => 'users.delete']);
         Permission::create(['name' => 'users.entorno']);
         Permission::create(['name' => 'users.avatar']); //
         Permission::create(['name' => 'users.perfil']); //
         Permission::create(['name' => 'users.updateImage']); //
         Permission::create(['name' => 'users.mycursadas']); //

         Permission::create(['name' => 'articles.store']);
         Permission::create(['name' => 'articles.update']);
         Permission::create(['name' => 'articles.delete']);

         Permission::create(['name' => 'comments.update']);
         Permission::create(['name' => 'comments.show']);
         Permission::create(['name' => 'comments.store']); //
         Permission::create(['name' => 'comments.delete']); //

         Permission::create(['name' => 'course.store']);
         Permission::create(['name' => 'course.index']);
         Permission::create(['name' => 'course.update']);
         Permission::create(['name' => 'course.delete']);
 
         Permission::create(['name' => 'lectivo.store']);
         Permission::create(['name' => 'lectivo.index']);
         Permission::create(['name' => 'lectivo.delete']);

         Permission::create(['name' => 'cursada.store']);
         Permission::create(['name' => 'cursada.index']);
         Permission::create(['name' => 'cursada.show']);
         Permission::create(['name' => 'cursada.update']);
         Permission::create(['name' => 'cursada.delete']);
         
         Permission::create(['name' => 'category.store']);
         Permission::create(['name' => 'category.update']);
         Permission::create(['name' => 'category.delete']);

         Permission::create(['name' => 'inscription.store']);
         Permission::create(['name' => 'inscription.index']);
         Permission::create(['name' => 'inscription.update']);
         Permission::create(['name' => 'inscription.delete']);

         Permission::create(['name' => 'pagos.store']);
         Permission::create(['name' => 'pagos.index']);
         Permission::create(['name' => 'pagos.create']);
         Permission::create(['name' => 'pagos.update']);
         Permission::create(['name' => 'pagos.delete']);
         //Admin
         $admin = Role::create(['name' => 'Admin']);
         $admin->givePermissionTo([
             'roles.show',
             'roles.index',
             'users.register',
             'users.index',
             'users.update',
             'users.delete',
             'users.entorno',
             'users.avatar', //comun
             'users.perfil', //comun
             'users.updateImage', //commun
             'articles.store',
             'articles.update',
             'articles.delete',
             'comments.update',
             'comments.show',
             'comments.store', //comun
             'comments.delete', //commun
             'course.store',
             'course.index',
             'course.update',
             'course.delete',
             'lectivo.store',
             'lectivo.index',
             'lectivo.delete',
             'cursada.store',
             'cursada.index',
             'cursada.show',
             'cursada.update',
             'cursada.delete',
             'category.store',
             'category.update',
             'category.delete',
             'inscription.store',
             'inscription.index',
             'inscription.update',
             'inscription.delete',
             'pagos.store',
             'pagos.index',
             'pagos.create',
             'pagos.update',
             'pagos.delete',
         ]);
         //$admin->givePermissionTo(Permission::all());

         //Student
         $student = Role::create(['name' => 'Alumno']);
         $student->givePermissionTo([
            'users.avatar', //comun
            'users.perfil', //comun
            'users.updateImage', //commun
            'comments.store', //comun
            'comments.delete', //commun
            'users.mycursadas',
         ]);
         
         //Teacher
         $teacher = Role::create(['name' => 'Profesor']);
         $student->givePermissionTo([ 
            'users.avatar', //comun
            'users.perfil', //comun
            'users.updateImage', //commun
            'comments.store', //comun
            'comments.delete', //commun
         ]);
    }
}
