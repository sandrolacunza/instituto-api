<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Inscription;
class InscriptionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Inscription::truncate();

        $faker = \Faker\Factory::create();
        for ($i = 0; $i < 3; $i++) {
            Inscription::create([
            'cursada_id' => $faker->numberBetween(1, 10),
            'user_id' => $faker->numberBetween(1, 10),
            'becado' => $faker->numerify('0.##'),
            ]);
        }
    }
}
