<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Profile;
use App\Models\Avatar;
use App\Models\Category;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Vaciar la tabla
        User::truncate();
        $faker = \Faker\Factory::create();
        // Crear la misma clave para todos los usuarios
        // conviene hacerlo antes del for para que el seeder
        // no se vuelva lento.
        $password = Hash::make('Unodos34!');
        $userAdmin = User::create([
            'name' => 'Admin Fulanito',
            'email' => 'admin@institutolya.com.ar',
            'password' => $password,
            
            
        ]);
        $userAdmin -> assignRole('Admin');
        Profile::create([
            'user_id' => $userAdmin->id,
            'dni' => 1000000,
            'fecha_nac' => '1985/05/07',
            'localidad' =>  'Resistencia',
            'provincia' => 'Chaco',
            'barrio' => 'Centro',
            'domicilio' => 'Arturo Illia 730',
            'telefono' => 3624276033,
        ]);
        Avatar::create([
            'user_id' => $userAdmin->id,
            'avatar' => 'user_default.png',
        ]);

        $userAlumno = User::create([
            'name' => 'Alumno Fulanito',
            'email' => 'student@institutolya.com.ar',
            'password' => $password,
           
        ]);
        $userAlumno -> assignRole('Alumno');
        Profile::create([
            'user_id' => $userAlumno->id,
            'dni' => 20000000,
            'fecha_nac' => '1985/05/07',
            'localidad' =>  'Resistencia',
            'provincia' => 'Chaco',
            'barrio' => 'Centro',
            'domicilio' => 'Arturo Illia 730',
            'telefono' => 3624276033,
        ]);
        Avatar::create([
            'user_id' => $userAlumno->id,
            'avatar' => 'user_default.png',
        ]);

        $userProfesor = User::create([
            'name' => 'Profesor Fulanito',
            'email' => 'teacher@institutolya.com.ar',
            'password' => $password,
           
        ]);
        $userProfesor -> assignRole('Profesor');
        Profile::create([
            'user_id' => $userProfesor->id,
            'dni' => 30000000,
            'fecha_nac' => '1985/05/07',
            'localidad' =>  'Resistencia',
            'provincia' => 'Chaco',
            'barrio' => 'Centro',
            'domicilio' => 'Arturo Illia 730',
            'telefono' => 3624276033,
        ]);
        Avatar::create([
            'user_id' => $userProfesor->id,
            'avatar' => 'user_default.png',
        ]);
        // Generar algunos usuarios para nuestra aplicacion
        for ($i = 0; $i < 10; $i++) {
            $user = User::create([
                'name' => $faker->name,
                'email' => $faker->email,
                'password' => $password,
            ]);
            $user->assignRole('Alumno');
            Profile::create([
                'user_id' => $user->id,
                'dni' => $faker->numberBetween($min = 1000000,$max = 50000000),
                'fecha_nac' => $faker->date($format = 'Y-m-d', $max = 'now'),
                'localidad' =>  'Resistencia',
                'provincia' => 'Chaco',
                'barrio' => $faker->city,
                'domicilio' => $faker->address,
                'telefono' => $faker->phoneNumber,
            ]);
            Avatar::create([
                'user_id' => $user->id,
                'avatar' => 'user_default.png',
            ]);
            $user->categories()->saveMany(
                $faker->randomElements(
                array(
                    Category::find(1),
                    Category::find(2),
                    Category::find(3)
                ), $faker->numberBetween(1, 3), false)
            );
        }
    }
}

