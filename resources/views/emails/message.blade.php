@component('mail::layout')
{{-- Header --}}
@slot('header')
@component('mail::header', ['url' => config('app.url')])
 Instituto L&A de Lenguas y Arte 
@endcomponent
@endslot

<img style="margin-right:auto; margin-left:auto;margin-bottom:20px;" src="https://dl.dropboxusercontent.com/s/qrpew8dwp7mvpt0/logo.png?dl=0"  alt='Logo' data-default="placeholder" data-max-width="52" data-customIcon="true"></img>
{{-- Body --}}
{{ $slot }}

{{-- Footer --}}
@slot('footer')
@component('mail::footer')
© {{ date('Y') }} {{ config('app.name') }}. Todos los derechos reservados.

Si no deseas recibir más correos, puedes [modificar tus preferencias][unsubscribe].

[unsubscribe]:  https://institutolya.com.ar/unsuscribe
@endcomponent
@endslot

[logo]: https://dl.dropboxusercontent.com/s/bqhvx2uinfvrm2m/logo.svg?dl=0
@endcomponent