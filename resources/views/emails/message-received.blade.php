<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Mensaje Recibido</title>
</head>
<body>
    Contenido del mensaje:
    <p><strong>Recibiste un mensaje de:</strong> <?php echo $msg["name"] ?></p>
    <p><strong>Asunto:</strong> <?php echo $msg["motivo"] ?></p>
    <p><strong>Email de contacto:</strong> <?php echo $msg["email"] ?></p>
    <p><strong>Consulta:</strong> <?php echo $msg["text"] ?></p>
</body>
</html>