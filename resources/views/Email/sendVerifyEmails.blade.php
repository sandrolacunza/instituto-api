@component('emails.message')
# Verificación de E-mail

Hola, te hemos enviado este correo para que **verifiques tu email**. De esta forma vamos a estar
seguros de contar con este canal de comunicación contigo.

En el caso de que esto sea un error, desestima este correo. De lo contrario, tienes que seguir 
dos pasos: <br><br>
**Paso 1:** Presionar el siguiente botón y verifica tu correo electrónico, luego realiza el paso número 2.

@component('mail::button', [ 'url' => $url ])
    Verificar Email
@endcomponent

<br>**Paso 2:** Grandiosos, ya tiene tu cuenta activada. Ya puedes ingresar al siguiente link
 <a href="https://institutolya.com.ar">https://institutolya.com.ar</a> y disfrutar 
de tu cuenta con los siguientes datos de ingreso:

**Correo:** Tu correo de registro es este, asi que deberás usar el mismo.<br>
**Contraseña:** Tu contraseña tiene el siguientes formato **lya-dni**, es decir si tu dni es 12345678,
tu contraseña sería **lya-12345678**. 

Muchas Gracias<br>
Ana Mónica Pierozzi<br>
Titular y Directora Académica de la Institución<br>


Saludos, ¡que estés bien!
@endcomponent
