@component('emails.message')
# E-mail de cambio de contraseña

Hola {{ $user->name }}, hemos recibido un pedido de **cambio de contraseña** de este correo.

En el caso de que esto sea un error, desestima este correo. En el caso de que haya sido usted quien pidió
el cambio de contraseña debe presionar el siguiente boton y seguir los siguientes pasos.

@component('mail::button', [ 'url' => 'https://institutolya.com.ar/change-password/'.$token ])
    Cambiar contraseña
@endcomponent

Muchas Gracias<br>
Ana Mónica Pierozzi<br>
Titular y Directora Académica de la Institución<br>


Saludos, ¡que estés bien!
@endcomponent

