<?php

namespace App\Exceptions;
use App\Http\Controllers\Api\Resources\BaseApi;
use App\Http\Controllers\Api\Resources\ResponsePackage;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->reportable(function (Throwable $e) {
            //
        });
    }
    public function render($request, Throwable $exception)
    {
        $package = new ResponsePackage();
        if ($exception instanceof \Spatie\Permission\Exceptions\UnauthorizedException) {
            return $package
            ->setError('Usted no tiene autorización para realizar esta acción', BaseApi::HTTP_FORBIDDEN_ERROR)
                ->setData('errors', BaseApi::DEFAULT_AUTHORIZATION_ERROR)
                ->toResponse();
        }
        if($exception instanceof \Illuminate\Routing\Exceptions\InvalidSignatureException){
            return $package
            ->setError('Esta ruta ya no es valida', BaseApi::HTTP_FORBIDDEN_ERROR)
            ->setData('errors', BaseApi::ROUTE_NOT_VALID)
            ->toResponse();
        } 
    return parent::render($request, $exception);
    }
}
