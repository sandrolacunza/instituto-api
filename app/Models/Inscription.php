<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Inscription extends Model
{
    use HasFactory;

    protected $fillable = ['id','cursada_id', 'user_id', 'becado'];
    protected $table = 'cursada_user';

    public function cursada()
    {
        return $this->belongsTo(Cursada::class);
    }
    public function user()
    {
        return $this->belongsTo(User::class);
    }
    public function notes()
    {
    return $this->hasMany('App\Models\Note', 'inscription_id', 'id');
    }
   
}
