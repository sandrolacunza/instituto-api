<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cursada extends Model
{
    use HasFactory;
    protected $fillable = ['courses_id', 'lectivo_id','teacher_id', 'division', 'estado', 'price', 'inicio', 'fin'];
    protected $table = 'cursada';
    
    public function courses()
    {
        return $this->belongsTo(Course::class);
    }
    public function lectivo()
    {
        return $this->belongsTo(Lectivo::class);
    }
    public function teacher()
    {
        return $this->belongsTo(User::class, 'teacher_id', 'id');
    }
    public function users()
    {
        return $this->belongsToMany(User::class)->withPivot('id','becado');
    }
}
