<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Lectivo extends Model
{
    use HasFactory;
    protected $fillable = ['ano', 'inicio', 'fin'];
    protected $table = 'lectivo';
    
    public function cursos(){
        return $this->belongsToMany('\App\Models\Course','cursada')->withPivot('price');
    }
}
