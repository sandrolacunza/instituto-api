<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pago extends Model
{
    use HasFactory;

    protected $fillable = ['cursada_id', 'user_id', 'subtotal', 'descuento', 'concepto', 'total', 'estado'];
    protected $table = 'pagos';

    public function cursada()
    {
        return $this->belongsTo(Cursada::class);
    }
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
