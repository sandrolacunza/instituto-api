<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Note extends Model
{
    use HasFactory;

    protected $fillable = ['inscription_id','lectora', 'auditiva', 'escrita', 'oral', 'usenglish'];
    protected $table = 'notes';


}
