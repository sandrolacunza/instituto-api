<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    use HasFactory;
    protected $table = 'profiles';
    protected $fillable = [
        'user_id', 
        'dni' ,
        'fecha_nac' ,
        'localidad' ,
        'provincia',
        'barrio' ,
        'domicilio',
        'telefono',
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
    
}
