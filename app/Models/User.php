<?php



namespace App\Models;


use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Spatie\Permission\Traits\HasRoles;
use App\Notifications\VerifyEmails;

class User extends Authenticatable implements JWTSubject, MustVerifyEmail
{
    use HasFactory, Notifiable, HasRoles;



    /**

     * The attributes that are mass assignable.

     *

     * @var array

     */

    protected $fillable = [
        'id',
        'name',
        'email',
        'password',
        'email_verified_at'
    ];



    /**

     * The attributes that should be hidden for arrays.

     *

     * @var array

     */

    protected $hidden = [
        'password',
        'remember_token',
    ];



    /**

     * The attributes that should be cast to native types.

     *

     * @var array

     */

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()

    {
        return [];
    }

    public function articles()
    {
    return $this->hasMany('App\Models\Article');
    }

    public function comments()
    {
    return $this->hasMany('App\Models\Comment');
    }
   
    public function categories()
    {
    return $this->belongsToMany('App\Models\Category')->as('subscriptions')
    ->withTimestamps();;
    }

    public function avatar()
    {
    return $this->hasOne('App\Models\Avatar');
    }

    public function profile()
    {
    return $this->hasOne('App\Models\Profile');
    }

    public function cursadas()
    {
    return $this->belongsToMany('App\Models\Cursada')->with('courses');
    }

    public function coursesITeacher()
    {
    return $this->hasMany('App\Models\Cursada', 'teacher_id', 'id')->with('courses','lectivo');
    }

    /**
 * Send the email verification notification.
 *
 * @return void
 */
    public function sendEmailVerificationNotification()
    {
        $this->notify(new VerifyEmails); // my notification
    }
}

