<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class MessageToJobs extends Mailable
{
    use Queueable, SerializesModels;
    public $subject = 'Mensaje de sección Trabaja con Nosotros';
    public $msg;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($msg, $file, $filename)
    {
        $this->msg = $msg;
        $this->file = $file;
        $this->filename = $filename;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.message-tojobs')->attach( $this->file, [
            'as' => $this->filename,
            'mime' => 'application/pdf',
       ]);
    }
}
