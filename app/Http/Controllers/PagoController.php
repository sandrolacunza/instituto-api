<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Api\Resources\BaseApi;
use App\Http\Controllers\Api\Resources\ResponsePackage;
use App\Models\Cursada;
use App\Models\Pago;

class PagoController extends Controller
{
    private static $rules = [
        'cursada_id' => 'integer|exists:App\Models\Cursada,id',
        'user_id' => 'integer|exists:App\Models\User,id',
    ];
    private static $messages = [
        'integer' => 'El valor tiene que ser un entero',
        'exists' => 'Es valor en la tabla a la cual se referencia',
        'required' => 'El campo :attibute es requerido',
    ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $package = new ResponsePackage();
        $pagos = Pago::with(['user', 'cursada'])->get();
        return $package->setData('pagos', $pagos)
            ->toResponse();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $package = new ResponsePackage();
            $result = Cursada::has('users')->where('estado','=', 1)->with('users')->get();
            foreach ($result as $value) {
                $cursada_id = $value->id;
                $price = $value->price;
                $users = $value->users;
                foreach ($users as $user) {
                    Pago::firstOrCreate([
                        'cursada_id' => $cursada_id,
                        'user_id' => $user->id,
                        'subtotal' => $price,
                        'descuento' => $user->pivot->becado,
                        'concepto' => date("n"),
                        'total' => ($price * $user->pivot->becado),
                    ]);
                }
            }
            return $package
            ->setData('success', 'Las cuotas se generaron correctamente')
            ->toResponse();

    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $package = new ResponsePackage();
        $validator = Validator::make($request->all(), self::$rules, self::$messages);
        if ($validator->fails()) {
            return $package
            ->setError($validator->errors(), BaseApi::HTTP_CONFLICT)
            ->setData('errors', $validator->errors())
            ->toResponse();
        }
        try {
            $pago = Pago::Create([
                'cursada_id' => $request->cursada_id,
                'user_id' => $request->user_id,
                'subtotal' => $request->subtotal,
                'descuento' => $request->descuento,
                'concepto' => $request->concepto,
                'estado' => $request->estado,
                'total' => $request->total,
            ]);
            return $package
                ->setData('pago', $pago)
                ->toResponse();

        } catch (\Throwable $th) {
            return $package
            ->setError($th->getMessage(), BaseApi::HTTP_CONFLICT)
                ->setData('errors', $th->getMessage())
                ->toResponse();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Pago $pago)
    {
        $package = new ResponsePackage();
        try {
          $pago->update($request->all());
          return $package
          ->setData('pago', $pago)
          ->toResponse();
        } catch (\Exception $e) {
         return $package
         ->setError($e->getMessage(), BaseApi::HTTP_CONFLICT)
             ->setData('errors', $e->getMessage())
             ->toResponse();
      }
    }

    /**
     * Remove the specified resource from storage.
     * estado {
     * 0 = no pagado
     * 1 = pagado
     * 2 = anulado
     * }
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Pago $pago)
    {
        $package = new ResponsePackage();
        $pago = Pago::findOrFail($pago->id);
        $pago->estado = '2';
        $pago->save();
        return $package
            ->setData('success', 'El pago se anuló correctamente')
            ->toResponse();
    }
}
