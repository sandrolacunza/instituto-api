<?php

namespace App\Http\Controllers;

use App\Models\Lectivo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Api\Resources\BaseApi;
use App\Http\Controllers\Api\Resources\ResponsePackage;

class LectivoController extends Controller
{
    private static $rules = [
        'ano' => 'required|string|max:4|min:4|unique:lectivo',
    ];

    private static $messages = [
        'required' => 'El campo año es obligatorio.',
        'unique' => 'El valor de año ya  existe en la db',
        'min' => 'El campo año debe tener 4 dígitos',
        'max' => 'El valor de año debe tener 4 dígitos',
    ];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $package = new ResponsePackage();
        $lectivo = Lectivo::orderBy('ano', 'DESC')->get();
        return $package->setData('lectivos', $lectivo)
            ->toResponse();
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $package = new ResponsePackage();
        $validator = Validator::make($request->all(), self::$rules, self::$messages);
        if ($validator->fails()) {
            return $package
            ->setError($validator->errors(), BaseApi::HTTP_CONFLICT)
            ->setData('errors', $validator->errors())
            ->toResponse();
        }
        try {
            $lectivo = Lectivo::Create([
                'ano' => $request->ano,
                'inicio' => $request->inicio,
                'fin' => $request->final,
            ]);
            return $package
                ->setData('lectivo', $lectivo)
                ->toResponse();

        } catch (\Throwable $th) {
            return $package
            ->setError($th->getMessage(), BaseApi::HTTP_CONFLICT)
                ->setData('errors', $th->getMessage())
                ->toResponse();
        }
    }

    
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Lectivo $lectivo)
    {
        $package = new ResponsePackage();
        try {
          $lectivo->update($request->all());
          return $package
          ->setData('lectivo', $lectivo)
          ->toResponse();
        } catch (\Exception $e) {
         return $package
         ->setError($e->getMessage(), BaseApi::HTTP_CONFLICT)
             ->setData('errors', $e->getMessage())
             ->toResponse();
      }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Lectivo $lectivo)
    {
        $package = new ResponsePackage();
            $lectivo = Lectivo::findOrFail($lectivo->id);
            $lectivo->delete();
            return $package
                ->setData('success', 'El año lectivo se borro correctamente')
                ->toResponse();
    }
}
