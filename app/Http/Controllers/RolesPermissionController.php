<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Api\Resources\BaseApi;
use App\Http\Controllers\Api\Resources\ResponsePackage;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\Models\User;
use Spatie\Permission\Traits\HasRoles;

class RolesPermissionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $package = new ResponsePackage();
        $roles = Role::all();
        return $package->setData('roles', $roles)
            ->toResponse();
    }

    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $role = Role::create(['name' => $request->name ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Role $role)
    {
        $package = new ResponsePackage();
        $role = Role::find($role->id);
        return $package->setData('roles', $role->permissions)->toResponse();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function activeDeactive(Request $request) {
        if (Auth::id() != $request->user_id) {
          $user = User::findOrFail($request->user_id);
          $user->active = !$user->active;
          $user->save();
          return redirect()->route("admin.users.index")->with('success', $user->name." status has been changed!");
        } else  {
          return redirect()->back()->withErrors(['You can\'t change your status!']);
        }
      }
     
      public function changeRole(Request $request) {
        if (Auth::id() != $request->user_id) {
          $user = User::findOrFail($request->user_id);
          $user->syncRoles($request->role);
          return redirect()->route("admin.users.index")->with('success', $user->name." role has been changed!");
        } else  {
          return redirect()->back()->withErrors(['You can\'t change your role!']);
        }
      }
}
