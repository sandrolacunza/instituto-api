<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Note;
use App\Models\Inscription;
use App\Http\Controllers\Api\Resources\BaseApi;
use App\Http\Controllers\Api\Resources\ResponsePackage;
class NoteController extends Controller
{
    public function index(Request $request, Inscription $inscription)
    {
        $package = new ResponsePackage();
        $notes = Note::where('inscription_id', $inscription->id)->get();
        return $package->setData('notes', $notes)
            ->toResponse();
    }

    public function update(Request $request, Note $note) {
        $note->update($request->all());
        return response()->json($note, 200);
    }
}
