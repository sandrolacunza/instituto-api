<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Api\Resources\BaseApi;
use App\Http\Controllers\Api\Resources\ResponsePackage;
use App\Models\Cursada;
use App\Models\User;
use App\Models\Inscription;
use App\Models\Note;
use App\Models\Pago;
class InscriptionController extends Controller
{
    private static $rules = [
        'cursada_id' => 'integer|exists:App\Models\Cursada,id',
        'user_id' => 'integer|exists:App\Models\User,id',
    ];
    private static $messages = [
        'integer' => 'El valor tiene que ser un entero',
        'exists' => 'Es valor en la tabla a la cual se referencia',
        'required' => 'El campo :attibute es requerido',
    ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $package = new ResponsePackage();
        $inscriptions = Inscription::with(['user', 'cursada', 'notes'])->get();
        return $package->setData('inscription', $inscriptions)
            ->toResponse();
    }

    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $package = new ResponsePackage();
        $validator = Validator::make($request->all(), self::$rules, self::$messages);
        if ($validator->fails()) {
            return $package
            ->setError($validator->errors(), BaseApi::HTTP_CONFLICT)
            ->setData('errors', $validator->errors())
            ->toResponse();
        }
        try {
            $inscription = Inscription::Create([
                'cursada_id' => $request->cursada_id,
                'user_id' => $request->user_id,
                'becado' => $request->becado,
            ]);
            for ($i=0; $i < 5; $i++) { 
                Note::Create([
                    'inscription_id' => $inscription->id,
                ]);
            }
            
            Pago::firstOrCreate([
                'cursada_id' => $request->cursada_id,
                'user_id' => $request->user_id,
                'subtotal' => $request->price_matricula,
                'descuento' => '1',
                'concepto' => '0',
                'total' => $request->price_matricula,
            ]);

            return $package
                ->setData('inscription', $inscription)
                ->toResponse();

        } catch (\Throwable $th) {
            return $package
            ->setError($th->getMessage(), BaseApi::HTTP_CONFLICT)
                ->setData('errors', $th->getMessage())
                ->toResponse();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

   

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Inscription $inscription)
    {
        $package = new ResponsePackage();
        try {
          $inscription->update($request->all());
          return $package
          ->setData('inscription', $inscription)
          ->toResponse();
        } catch (\Exception $e) {
         return $package
         ->setError($e->getMessage(), BaseApi::HTTP_CONFLICT)
             ->setData('errors', $e->getMessage())
             ->toResponse();
      }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Inscription $inscription)
    {
        $package = new ResponsePackage();
        $inscription = Inscription::findOrFail($inscription->id);
        $inscription->delete();
        return $package
            ->setData('success', 'La inscripción se borro correctamente')
            ->toResponse();
    }

    // public function probandoindex(Request $request) {
    //     $package = new ResponsePackage();
    //     if(env('CRON_TOKEN') == $request->cron_token) {
    //         $result = Cursada::has('users')->where('estado','=', 1)->with('users')->get();
    //         foreach ($result as $value) {
    //             $cursada_id = $value->id;
    //             $price = $value->price;
    //             $users = $value->users;
    //             foreach ($users as $user) {
    //                 Pago::Create([
    //                     'cursada_id' => $cursada_id,
    //                     'user_id' => $user->id,
    //                     'subtotal' => $price,
    //                     'descuento' => $user->pivot->becado,
    //                     'concepto' => date("n"),
    //                     'total' => ($price * $user->pivot->becado),
    //                     'estado' => '0',
    //                 ]);
    //             }
    //         }
    //         return $package
    //         ->setData('success', 'Las cuotas se generaron correctamente')
    //         ->toResponse();
    //     }else {
    //         return $package
    //         ->setError(BaseApi::HTTP_CONFLICT)
    //             ->setData('errors', 'No presenta las credenciales correspondientes')
    //             ->toResponse();
    //     }

    // }
}
