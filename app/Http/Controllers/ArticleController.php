<?php



namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Models\Article;
use App\Http\Resources\Article as ArticleResource;
use App\Http\Resources\ArticleCollection;
use App\Models\Comment;
use App\Http\Controllers\Api\Resources\BaseApi;
use App\Http\Controllers\Api\Resources\ResponsePackage;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;


class ArticleController extends Controller
{
    public function __construct()
    {
        // Necesitamos obtener una instancia de la clase Client la cual tiene algunos métodos
        // que serán necesarios.
        $this->dropbox = Storage::disk('dropbox')->getDriver()->getAdapter()->getClient();   
    }
    private static $messages = [
        'required' => 'El campo :attribute es obligatorio.',
        'unique' => 'El valor de :attribute ya  existe en la db',
        'max' => 'el campo :attribute no soporta más de 255 caracteres',
        'exists' => 'El id de :attribute no existe en la tabla'
    ];

 /**
 * @OA\Get(
 *     path="/articles",
 *     @OA\Response(response="200", description="Display a listing of projects.")
 * )
 */

     
    public function index()
    {
        $package = new ResponsePackage();
        $articles = Article::orderBy('id', 'DESC')->get();
        return $package->setData('articles', $articles)
            ->toResponse();
    }

    public function show(Article $article)
    {
        return new ArticleResource($article);
    }

    public function image(Article $article)
    {

            // Storage::disk('images')->getDriver()->getAdapter()->applyPathPrefix($article->image), $article->image);
             return Storage::disk('dropbox')->download($article->image);
    }

    public function store(Request $request)
    {
       $validator = Validator::make($request->all(), [
            'title' => 'required|string|unique:articles|max:255',
            'body' => 'required|string',
            'category_id' => 'required|exists:categories,id',
            'image' => 'required|image',
        ], self::$messages);
        if ($validator->fails()) {
            return response()->json([
                'error' => 'Los datos no pudieron ser validados',
                "errors" => $validator->errors()
            ], 400);
        }
        
        Storage::disk('dropbox')->putFileAs(
            '/', 
            $request->file('image'), 
            $request->file('image')->getClientOriginalName()
        );
        // Creamos el enlace publico en dropbox utilizando la propiedad dropbox
        // definida en el constructor de la clase y almacenamos la respuesta.
        $response = $this->dropbox->createSharedLinkWithSettings(
            $request->file('image')->getClientOriginalName(), 
            ["requested_visibility" => "public"]
        );
        $article = new Article($request->all());
        $article->image = $response['name'];
        $article->save();
        return response()->json(new ArticleResource($article), 201);
    
    }


    public function update(Request $request, Article $article)
    {
        $request->validate([
            'title' => 'required|string|unique:articles,title,' . $article->id . '|max:255',
            'body' => 'required',
            'category_id' => 'required|exists:categories,id'
        ], self::$messages);
        $article->update($request->all());
        return response()->json($article, 200);
    }

    public function delete(Request $request, Article $article)
    {
        $article->delete();
        return response()->json(['success' => 'Se elimino correctamente'], 200);
    }
    
}

