<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\UpdatePasswordRequest;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use App\Http\Controllers\Api\Resources\BaseApi;
use App\Http\Controllers\Api\Resources\ResponsePackage;

class ResetPasswordController extends Controller {

    public function passwordResetProcess(UpdatePasswordRequest $request){
      return $this->updatePasswordRow($request)->count() > 0 ? $this->resetPassword($request) : $this->tokenNotFoundError();
    }

    // Verify if token is valid
    private function updatePasswordRow($request){
       return DB::table('password_resets')->where([
           'email' => $request->email,
           'token' => $request->passwordToken
       ]);
    }

    // Token not found response
    private function tokenNotFoundError() {
      $package = new ResponsePackage();

      return $package->setError(
        BaseApi::PASSWORD_RESET_INVALID_TOKENOREMAIL,
        BaseApi::HTTP_CONFLICT
    )
        ->setData('errors', BaseApi::PASSWORD_RESET_INVALID_TOKENOREMAIL)
        ->toResponse();
    }

    // Reset password
    private function resetPassword($request) {
       $package = new ResponsePackage();
        // find email
        $userData = User::whereEmail($request->email)->first();
        // update password
        $userData->update([
          'password'=>bcrypt($request->password)
        ]);
        // remove verification data from db
        $this->updatePasswordRow($request)->delete();

        // reset password response
        return $package
            ->setData('message', 'La password fue actualizada correctamente.')
            ->toResponse();
    }    

}