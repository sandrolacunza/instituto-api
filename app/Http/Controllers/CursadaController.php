<?php

namespace App\Http\Controllers;

use App\Models\Cursada;
use App\Models\Course;
use App\Models\Lectivo;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Api\Resources\BaseApi;
use App\Http\Controllers\Api\Resources\ResponsePackage;

class CursadaController extends Controller
{
    private static $rules = [
        'courses_id' => 'nullable|integer|exists:App\Models\Course,id',
        'lectivo_id' => 'nullable|integer|exists:App\Models\Lectivo,id',
        'price' => 'numeric|min:0'
    ];

    private static $messages = [
        'integer' => 'El valor tiene que ser un entero',
        'exists' => 'Es valor en la tabla a la cual se referencia',
        'min' => 'El campo :attibute no puede tener ese valor',
    ];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $package = new ResponsePackage();
        $cursadas = Cursada::with(['courses', 'lectivo', 'teacher'])->orderBy('id', 'DESC')->get();
        return $package->setData('cursadas', $cursadas)
            ->toResponse();
    }

    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $package = new ResponsePackage();
        $validator = Validator::make($request->all(), self::$rules, self::$messages);
        if ($validator->fails()) {
            return response()->json([
                'error' => 'Los datos no pudieron ser validados',
                "errors" => $validator->errors()
            ], 400);
        }
        //verificamos si ya existe esa combinación de curso, año lectivo y división
        $cursada = Cursada::where(
            'courses_id', $request->course_id)
            ->where('lectivo_id', $request->lectivo_id)
            ->where('division', $request->division)->count();
        if($cursada > 0) {
            return $package
                ->setError(BaseApi::CURSADA_EXIST, BaseApi::HTTP_CONFLICT)
                ->setData('errors', BaseApi::CURSADA_EXIST)
                ->toResponse();
        }
        //si pasa todas las validadaciónes creamos la cursada
        try {
            $cursada = Cursada::Create([
                'courses_id' => $request->course_id,
                'lectivo_id' => $request->lectivo_id,
                'teacher_id' => $request->teacher_id,
                'division' => $request->division,
                'estado' => $request->estado,
                'price' => $request->price,
                'inicio' => $request->inicio,
                'fin' => $request->fin,
            ]);
            return response()->json($cursada, 200);
        } catch (\Throwable $th) {
            return response()->json($th, 400);;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Cursada $cursada)
    {
        // $curso = Course::find($cursada->course_id);
        // $lectivo = Lectivo::find($cursada->lectivo_id);
        // return response()->json([$cursada, $lectivo], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Cursada $cursada)
    {
        $package = new ResponsePackage();
        try {
          $cursada->update($request->all());
          return $package
          ->setData('cursada', $cursada)
          ->toResponse();
        } catch (\Exception $e) {
         return $package
         ->setError($e->getMessage(), BaseApi::HTTP_CONFLICT)
             ->setData('errors', $e->getMessage())
             ->toResponse();
      }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Cursada $cursada)
    {
        $package = new ResponsePackage();
        $cursada = Cursada::findOrFail($cursada->id);
        $cursada->delete();
        return $package
            ->setData('success', 'La cursada se borro correctamente')
            ->toResponse();
    }
    public function returnLetter($num){
        $div = ['A', 'B', 'C', 'D', 'E'];
        $divToReturn = [];
        for ($i = 0; $i < $num; $i++) {
            array_push($divToReturn, $div[$i]);
        }
        return $divToReturn;
    }

    public function division(Request $request)
    {
        $package = new ResponsePackage();
        $cursada = Cursada::where('lectivo_id', $request->lectivo_id)
        ->where('courses_id', $request->course_id)->count();
        $letter = $this->returnLetter($cursada);
        return $package
            ->setData('divisiones', $letter)
            ->toResponse();
    }
    
    
}
