<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
// use Illuminate\Routing\UrlGenerator;
// use Illuminate\Support\Facades\URL;
// use Illuminate\Routing\Middleware\ValidateSignature;
use App\Http\Controllers\Api\Resources\BaseApi;
use App\Http\Controllers\Api\Resources\ResponsePackage;

class VerificationController extends Controller
{
    /**
     * Show the email verification notice.
     *
     */
    public function show()
    {
        //
    }

    /**
     * Mark the authenticated user's email address as verified.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function verify($user_id, Request $request)
    {
        $package = new ResponsePackage();
        if (!$request->hasValidSignature()) {

            return $package->setError(
                BaseApi::HTTP_FORBIDDEN_ERROR,
                BaseApi::HTTP_CONFLICT
            )
                ->setData('errors', BaseApi::ROUTE_NOT_VALID)
                ->toResponse();
        }
    
        $user = User::findOrFail($user_id);
    
        if (!$user->hasVerifiedEmail()) {
            $user->markEmailAsVerified();
            return $package->setData('message', BaseApi::EMAIL_WAS_VERIFIED)
            ->toResponse();
        } else {
            return $package->setError(
                BaseApi::HTTP_FORBIDDEN_ERROR,
                BaseApi::HTTP_CONFLICT
            )
                ->setData('errors', BaseApi::EMAIL_HAS_VERIFICATION)
                ->toResponse();
        }
    

//        return redirect($this->redirectPath());
    }

    /**
     * Resend the email verification notification.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function resend($user_id, Request $request)
    {
        $package = new ResponsePackage();
        $user = User::findOrFail($user_id);
        if ($user->hasVerifiedEmail()) {
            return $package->setError(
                BaseApi::HTTP_FORBIDDEN_ERROR,
                BaseApi::HTTP_CONFLICT
            )
                ->setData('errors', BaseApi::EMAIL_HAS_VERIFICATION)
                ->toResponse();
        }
        $user->sendEmailVerificationNotification();
        return $package->setData('message', BaseApi::EMAIL_VERIFICATION_RESEND)
            ->toResponse();
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth')->except('verify', 'resend');
        $this->middleware('signed')->only('verify');
        $this->middleware('throttle:6,1')->only('verify', 'resend');
    }
}