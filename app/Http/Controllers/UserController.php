<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Inscription;
use App\Models\Profile;
use App\Models\Avatar;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use App\Http\Controllers\Api\Resources\BaseApi;
use App\Http\Controllers\Api\Resources\ResponsePackage;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
use App\http\Resources\User as UserResource;
use App\http\Resources\UserCollection;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Auth\Events\Registered;

use Illuminate\Support\Facades\Mail;
use App\Mail\MessageUserRegister;
use Illuminate\Support\Facades\DB;




class UserController extends Controller
{
    public function __construct()
    {
        // Necesitamos obtener una instancia de la clase Client la cual tiene algunos métodos
        // que serán necesarios.
        $this->dropbox = Storage::disk('dropbox')->getDriver()->getAdapter()->getClient();   
    }
    
    public function setEnv(Request $request)
    {
        $name_v = $request->get('name');
        $value = $request->get('value');
        $path = base_path('.env');
        if (file_exists($path)) {
            file_put_contents($path, str_replace(
                $name_v . '=' . env($name_v), $name_v . '=' . $value, file_get_contents($path)
            ));
            
        }
    }
    private static $rules = [
        'name' => 'required|string|max:255',
        'email' => 'required|string|email|max:255|unique:users',

    ];
    //'email' => 'required|string|email|max:255|unique:users',
    public static $updateRules = [
        'localidad' =>  'required|string|max:100',
        'provincia' => 'required|string|max:100',
        'barrio' => 'required|string|max:50',
        'domicilio' => 'required|string|max:150',
        'telefono' => 'required|string|max:13',

    ];

    private static $messages = [
        'required' => 'El campo :attribute es obligatorio.',
        'unique' => 'El valor de :attribute ya  existe en la db',
        'max' => 'el campo :attribute no soporta más de 255 caracteres',
        'min' => 'el campo :attribute debe contener no menos de 6 caracteres',

    ];

   
    public function index()
    {
        $package = new ResponsePackage();
        $usuarios = User::with(['profile', 'avatar'])->get();
        return $package->setData('usuarios', $usuarios)
            ->toResponse();
    }

    public function show(User $user)
    {
        return new UserResource($user);
    }

    public function authenticate(Request $request)
    {
        $package = new ResponsePackage();
        $credentials = $request->only('email', 'password');

        $myTTL = 60; //minutes
        JWTAuth::factory()->setTTL($myTTL);
        if (!$token = JWTAuth::attempt($credentials)) {
            return $package->setError(
                BaseApi::INVALID_EMAIL_PASSWORD,
                BaseApi::HTTP_CONFLICT
            )
                ->setData('errors', BaseApi::INVALID_EMAIL_PASSWORD)
                ->toResponse();
        }
        $usuario = JWTAuth::user();

        if (!$usuario->hasRole($request->role)) {
            return $package->setError(
                BaseApi::INVALID_ROLE,
                BaseApi::HTTP_CONFLICT
            )
                ->setData('errors', BaseApi::INVALID_ROLE)
                ->toResponse();
        }


        $avatar = Avatar::where('user_id', $usuario->id)->first();
        $adapter = Storage::disk('dropbox')->getAdapter();
        $client = $adapter->getClient();
        $link = $client->getTemporaryLink($avatar->avatar);
        $roles = $usuario->roles->pluck('name');
        return response()->json(array('usuario' => $usuario, 'profile' => $usuario->profile ,'token' => $token, 'roles' => $roles, 'avatar' => $link));
    }

    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), self::$rules, self::$messages);
        if ($validator->fails()) {
            return response()->json([
                'error' => 'Los datos no pudieron ser validados',
                "errors" => $validator->errors()
            ], 400);
        }
        $user = User::create([
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'password' => Hash::make('lya-' . $request->dni),
        ]);
        Profile::create([
            'user_id' =>$user->id,
            'dni' => $request->dni,
            'fecha_nac' => $request->fecha_nac,
            'localidad' =>  $request->localidad,
            'provincia' => $request->provincia,
            'barrio' => $request->barrio,
            'domicilio' => $request->domicilio,
            'telefono' => $request->telefono,
        ]);
        Avatar::create([
            'user_id' =>$user->id,
        ]);
        $user->syncRoles($request->role);
        $user->sendEmailVerificationNotification();
        //event(new Registered($user));
        $token = JWTAuth::fromUser($user);
        //Mail::to($request->email)->send(new MessageUserRegister($user));
        return response()->json(compact('user', 'token'), 201);
    }

    public function getAuthenticatedUser()
    {
        try {
            if (!$user = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['user_not_found'], 404);
            }
        } catch (TokenExpiredException $e) {
            return response()->json(['token_expired'], $e->getCode());
        } catch (TokenInvalidException $e) {
            return response()->json(['token_invalid'], $e->getCode());
        } catch (JWTException $e) {
            return response()->json(['token_absent'], $e->getCode());
        }
        return response()->json(compact('user'));
    }

    public function logout()
    {
        try {
            JWTAuth::invalidate(JWTAuth::getToken());
            return response()->json([
                "status" => "success",
                "message" => "El usuario cerro sesión "
            ], 200);
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json(["message" => "No se pudo cerrar la sesión."], 500);
        }
    }
    public function delete($id)
    {
        $usuario = User::find($id);
        if($usuario) {
            $usuario->delete();
            return response()->json(['success' => 'El usuario se elimino correctamente'], 200);
        } else {
            return response()->json(['errors' => 'No se encuentra el usuario'], 400);
        }
        // $user->delete();
    }
    //endpoint to update student's profile
    public function perfil(Request $request)
    {
        $package = new ResponsePackage();
        //self::$updateRules['email'] = self::$updateRules['email'] . ',email,' . $request->id;
         $validator = Validator::make($request->all(), self::$updateRules, self::$messages);
         if ($validator->fails()) {
             return response()->json([
                 'error' => 'Los datos no pudieron ser validados',
                 "errors" => $validator->errors()
             ], 400);
         }
         $user = JWTAuth::user();
         $profile = Profile::where('user_id', $user->id)->first();
         $avatar = Avatar::where('user_id', $user->id)->first();
         $profile->provincia = $request->provincia;
         $profile->localidad = $request->localidad;
         $profile->barrio = $request->barrio;
         $profile->domicilio = $request->domicilio;
         $profile->telefono = $request->telefono;
         $profile->save();
         $adapter = Storage::disk('dropbox')->getAdapter();
         $client = $adapter->getClient();
         $link = $client->getTemporaryLink($avatar->avatar);
         $token = JWTAuth::fromUser($user);
         $roles = $user->roles->pluck('name');
        return response()->json(array('usuario' => $user, 'profile' => $user->profile ,'token' => $token, 'roles' => $roles, 'avatar' => $link));
    }
    //endpoint to get student's courses
    public function myCursadas(Request $request)
    {
        $package = new ResponsePackage();
        $user = JWTAuth::user();
        // //$user = User::find(1);
        // $cursadas = $user->cursadas()->get();

        $cursadas = Inscription::join('cursada', 'cursada_user.cursada_id', '=', 'cursada.id')
                ->join('courses', 'courses.id', '=', 'cursada.courses_id')
                ->join('lectivo', 'lectivo.id', '=', 'cursada.lectivo_id')
                ->select('cursada_user.*', 'courses.name', 'lectivo.ano', 'cursada.estado')
                ->where('cursada_user.user_id', '=', $user->id)
                ->with('notes')
                ->get();
                
        return $package
            ->setData('cursadas', $cursadas)
            ->toResponse();
    }

    public function avatar(User $user)
    {
        $avatar = Avatar::where('user_id',$user->id)->first();
        return Storage::disk('dropbox')->download($avatar->avatar);
    }

    public function updateImage(Request $request)
    {
        $package = new ResponsePackage();
        $user = JWTAuth::user();

        $nombre_archivo = $request->file('avatar')->getClientOriginalName();

        list($nombre, $ext) = explode(".", $nombre_archivo);
        $fecha = date("YmdHis"); 
        $renombre = $nombre . $fecha .'.'. $ext;
        $avatar = Avatar::where('user_id',$user->id)->first();
        Storage::disk('dropbox')->putFileAs(
            '/', 
            $request->file('avatar'), 
            $renombre
        );
        // Creamos el enlace publico en dropbox utilizando la propiedad dropbox
        // definida en el constructor de la clase y almacenamos la respuesta.
        $response = $this->dropbox->createSharedLinkWithSettings(
            $renombre, 
            ["requested_visibility" => "public"]
        );
        $avatar->avatar = $response['name'];
        $avatar->save();
        $link = $this->dropbox->getTemporaryLink($avatar->avatar);
        $token = JWTAuth::fromUser($user);
        $roles = $user->roles->pluck('name');
       return response()->json(array('usuario' => $user, 'profile' => $user->profile ,'token' => $token, 'roles' => $roles , 'avatar' => $link));
    }

    public function update(Request $request, User $user)
    {
        $package = new ResponsePackage();
        $validator = Validator::make($request->all(), self::$updateRules, self::$messages);
        if ($validator->fails()) {
            return response()->json([
                'error' => 'Los datos no pudieron ser validados',
                "errors" => $validator->errors()
            ], 400);
        }
        $user->profile->update($request->all());
        return response()->json($user, 200);
    }

    public function refresh(Request $request)
    {
        $package = new ResponsePackage();
        $token = $request->bearerToken();
        if (empty($token)) {
            return $package
                ->setError(BaseApi::API_INVALID_REQUEST, BaseApi::HTTP_CONFLICT)
                ->setData('errors', BaseApi::API_TOKEN_NOT_PROVIDED)
                ->toResponse();
        }
        try {
                $refreshed_token = JWTAuth::refresh(JWTAuth::getToken());
                $user = JWTAuth::setToken($refreshed_token)->toUser();
            return $package
                ->setData('message', 'token_expired')
                ->setData('token', $refreshed_token)
                ->toResponse();
        } catch (TokenInvalidException $e) {
            return $package->setError($e->getMessage(), BaseApi::HTTP_CONFLICT)
                ->setData('errors', $e->getMessage())
                ->toResponse();
        } catch (JWTException $e) {
            return $package->setError($e->getMessage(), BaseApi::HTTP_CONFLICT)
                ->setData('errors', $e->getMessage())
                ->toResponse();
        } catch (\Exception $e) {
            return $package->setError($e->getMessage(), BaseApi::HTTP_CONFLICT)
                ->setData('errors', $e->getMessage())
                ->toResponse();
        }
    }

}

