<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Models\User;
use App\Mail\SendMail;
use Illuminate\Support\Facades\Mail;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use App\Http\Controllers\Api\Resources\BaseApi;
use App\Http\Controllers\Api\Resources\ResponsePackage;

class ForgotPasswordController extends Controller
{

    public function sendPasswordResetEmail(Request $request){
        $package = new ResponsePackage();
        // If email does not exist
        if(!$this->validEmail($request->email)) {
            return $package->setError(
                BaseApi::PASSWORD_RESET_INVALID_USER,
                BaseApi::HTTP_NOT_FOUND
            )
                ->setData('errors', BaseApi::PASSWORD_RESET_INVALID_USER)
                ->toResponse();

        } else {
            // If email exists
            $this->sendMail($request->email);
            return $package
            ->setData('message', 'Revise su correo, se le ha enviado un email para actualizar su password.')
            ->toResponse();     
        }
    }


    public function sendMail($email){
        $user = User::whereEmail($email)->first();
        $token = $this->generateToken($email);
        Mail::to($email)->send(new SendMail($token, $user));
    }

    public function validEmail($email) {
       return !!User::where('email', $email)->first();
    }

    public function generateToken($email){
      $isOtherToken = DB::table('password_resets')->where('email', $email)->first();

      if($isOtherToken) {
        return $isOtherToken->token;
      }

      $token = Str::random(80);;
      $this->storeToken($token, $email);
      return $token;
    }

    public function storeToken($token, $email){
        DB::table('password_resets')->insert([
            'email' => $email,
            'token' => $token,
            'created_at' => Carbon::now()            
        ]);
    }
}
