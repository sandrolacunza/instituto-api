<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Artisan;
use Illuminate\Http\Request;

class ApiController extends Controller
{
    public function getMigrar()
    {
        //$exitCode = Artisan::call('migrate:refresh',['--seed'=> true]);
        $exitCode = Artisan::call('migrate:refresh',['--seed'=>true]);
        return $exitCode;
    }
    public function getStorage()
    {
        $exitCode = Artisan::call('storage:link');
        return $exitCode;
    }
}
