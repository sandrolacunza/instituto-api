<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Cursada;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Http\Controllers\Api\Resources\BaseApi;
use App\Http\Controllers\Api\Resources\ResponsePackage;

class TeacherController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $package = new ResponsePackage();
        $teachers =  User::whereHas("roles", function($q){ $q->where("name", "Profesor"); })->get();
        return $package
            ->setData('teachers', $teachers)
            ->toResponse();
    }
    public function myCourses() {
        $package = new ResponsePackage();
        $user = JWTAuth::user();
        return $package
            ->setData('cursadas', $user->coursesITeacher)
            ->toResponse();
    }
    public function inscriptions(Request $request, Cursada $cursada) {
        $package = new ResponsePackage();
        return $package
        ->setData('inscriptions', $cursada->users)
        ->toResponse();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        //
    }
}
