<?php

namespace App\Http\Controllers;

use App\Models\Course;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Api\Resources\BaseApi;
use App\Http\Controllers\Api\Resources\ResponsePackage;

class CourseController extends Controller
{
    private static $rules = [
        'name' => 'required|string|max:255|unique:courses',
        'description' => 'required|string|max:255',
    ];

    private static $messages = [
        'required' => 'El campo :attribute es obligatorio.',
        'unique' => 'El valor de :attribute ya  existe en la db',
        'max' => 'el campo :attribute no soporta más de 255 caracteres',
    ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $package = new ResponsePackage();
        $courses = Course::all();
        return $package->setData('courses', $courses)
            ->toResponse();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $package = new ResponsePackage();
        $validator = Validator::make($request->all(), self::$rules, self::$messages);
        if ($validator->fails()) {
            return $package
                ->setError($validator->errors(), BaseApi::HTTP_CONFLICT)
                ->setData('errors', $validator->errors())
                ->toResponse();

        }
        try {
            $course = Course::Create([
                'name' => $request->name,
                'description' => $request->description,
                'category_id' => $request->category_id,
            ]);
            return $package
                ->setData('courses', $course)
                ->toResponse();

        } catch (\Throwable $th) {
            return $package
            ->setError($th->getMessage(), BaseApi::HTTP_CONFLICT)
                ->setData('errors', $th->getMessage())
                ->toResponse();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function show(Course $course)
    {
        //
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Course $course)
    {
        $package = new ResponsePackage();
           try {
             $course->update($request->all());
             return $package
             ->setData('courses', $course)
             ->toResponse();
           } catch (\Exception $e) {
            return $package
            ->setError($e->getMessage(), BaseApi::HTTP_CONFLICT)
                ->setData('errors', $e->getMessage())
                ->toResponse();
         }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function delete(Course $course)
    {
            $package = new ResponsePackage();
            $course = Course::findOrFail($course->id);
            $course->delete();
            return $package
                ->setData('success', 'El curso se borro correctamente')
                ->toResponse();
    }
}
