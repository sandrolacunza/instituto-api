<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Api\Resources\BaseApi;
use App\Http\Controllers\Api\Resources\ResponsePackage;

use Illuminate\Http\Request;
use App\Models\Category;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;

class CategoryController extends Controller
{
    public function __construct()
    {
        // Necesitamos obtener una instancia de la clase Client la cual tiene algunos métodos
        // que serán necesarios.
        $this->dropbox = Storage::disk('dropbox')->getDriver()->getAdapter()->getClient();   
    }

    private static $rules = [
        'name' => 'required|string',
        'description' => 'required|max:400',
        'image' => 'required'
    ];
    private static $messages = [
        'required' => 'El campo :attribute es obligatorio.',
        'max' => 'el campo :attribute no soporta más de 400 caracteres',

    ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $package = new ResponsePackage();
        $category = Category::all();
        return $package->setData('category', $category)
            ->toResponse();

    }

   /**
    * Function to download categorie's image
    */
    public function image(Category $category)
    {  
        // Storage::disk('images')->getDriver()->getAdapter()->applyPathPrefix($article->image), $article->image);
            return Storage::disk('dropbox')->download($category->image);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $package = new ResponsePackage();
        $validator = Validator::make($request->all(), self::$rules, self::$messages);
        if ($validator->fails()) {
            // return response()->json([
            //     'error' => 'Los datos no pudieron ser validados',
            //     "errors" => $validator->errors()
            // ], 400);
            return $package->setData('errors', $validator->errors())
                ->toResponse();

        }
        try {
            Storage::disk('dropbox')->putFileAs(
                '/', 
                $request->file('image'), 
                $request->file('image')->getClientOriginalName()
            );
            $response = $this->dropbox->createSharedLinkWithSettings(
                $request->file('image')->getClientOriginalName(), 
                ["requested_visibility" => "public"]
            );
            $category = new Category($request->all());
            $category->image = $response['name'];
            $category->save();
            return $package->setStatus(BaseApi::HTTP_CREATED)
            ->toResponse();

        } catch (\Exception $e) {
            return $package
            ->setError(BaseApi::CREATE_FAIL, BaseApi::HTTP_CONFLICT)
            ->setData('errors', $e->getMessage())
            ->toResponse();

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        $package = new ResponsePackage();
        return $package->setData('category', $category)
                ->setData('courses', $category->courses)
                ->toResponse();

    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
        try {
            $category->update($request->all());
            return response()->json($category, 200);
          } catch (\Exception $e) {
            return response()->json(['errors' => $e->getMessage()], 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Category $category)
    {
        $category = Category::findOrFail($category->id);
        $category->delete();
        return response()->json(['success' => 'La categoría se borro correctamente'], 200);
    }
}
