<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\MessageToJobs;
use App\Mail\MessageReceived;
use Illuminate\Support\Facades\Validator;

class ContactController extends Controller
{
    private static $contactRules = [
        'name' => 'required|string',
        'motivo' => 'required|string',
        'email' => 'required|email',
        'text' => 'required|max:400',
    ];
    private static $jobsRules = [
        'name' => 'required|string',
        'email' => 'required|email',
        'text' => 'required|max:400',
    ];
    private static $messages = [
        'required' => 'El campo :attribute es obligatorio.',
        'max' => 'el campo :attribute no soporta más de 400 caracteres',
        'email' => 'El campo email no tiene formato email',
    ];

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), self::$contactRules, self::$messages);
        if ($validator->fails()) {
            return response()->json([
                'error' => 'Los datos no pudieron ser validados',
                "errors" => $validator->errors()
            ], 400);
        }
        $msg = $request->all();
        Mail::to(env('MAIL_INSTITUCIONAL'))->send(new MessageReceived($msg));
        return ;
    }

    public function messagejobs(Request $request)
    {
        $validator = Validator::make($request->all(), self::$jobsRules, self::$messages);
        if ($validator->fails()) {
            return response()->json([
                'error' => 'Los datos no pudieron ser validados',
                "errors" => $validator->errors()
            ], 400);
        }
        $msg = $request->all();
        $file = $request->file('file');
        $filename = $request->file('file')->getClientOriginalName();
        Mail::to(env('MAIL_INSTITUCIONAL'))->send(new MessageToJobs($msg, $file, $filename));
        return ;
    }
}
