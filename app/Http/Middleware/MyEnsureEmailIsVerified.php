<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Api\Resources\BaseApi;
use App\Http\Controllers\Api\Resources\ResponsePackage;

class MyEnsureEmailIsVerified
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $package = new ResponsePackage();
        $user = Auth::user();
        //if (!$user || ($user instanceof MustVerifyEmail && !$user->hasVerifiedEmail())) {
        if (!$user->hasVerifiedEmail()) {
        // return $request->expectsJson()
        //         ? abort(403, 'Su email no se encuentra verificado.') // replace with your message, or path to translation
        //         : Redirect::route('verification.notice');
        return $package->setError(
            BaseApi::EMAIL_NOT_VERIFY,
            BaseApi::HTTP_CONFLICT
        )
            ->setData('errors', BaseApi::EMAIL_NOT_VERIFY)
            ->toResponse();
    }

    return $next($request);
    }
}
