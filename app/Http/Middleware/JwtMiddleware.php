<?php

namespace App\Http\Middleware;

use Closure;
use Exception;
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
use Tymon\JWTAuth\Http\Middleware\BaseMiddleware;

class JwtMiddleware extends BaseMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try {
            $user = JWTAuth::parseToken()->authenticate();
        } catch (TokenExpiredException $e) {
            return response()->json(['errors' => 'Su token ha expirado'], 401);
        } catch (TokenInvalidException $e) {
            return response()->json(['errors' => 'Usted no tiene un token válido'], 401);
        } catch (JWTException $e) {
            return response()->json(['errors' => 'Usted debe presentar un Token válido'], 401);
        } catch (Exception $e) {
            return response()->json(['errors' => $e->getMessage()], 500);
        }
        return $next($request);
    }
}
